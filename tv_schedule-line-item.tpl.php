<?php

/**
 * @file
 * Default theme implementation to display a schedule line item.
 *
 * Available variables:
 * - $series_title: the series/movie title of the schedule line item. If there
 *   is local series show related to it. It will be a link html to the node.
 * - $start_time: start time of the show.
 * - $start_time_arr is an array contains two item the time in hour and minutes
 *   format and ante like AM or PM.
 * - $schedule_date The scheduled date of the show.
 * - $media_entity_title: the episode name or movie and event name.
 * - $synopsis: The synopsis of the schedule line item.
 * - $local_show_id: the related local show node id if it exists.
 * - $element: an array of raw data. $element['child'] contain all the raw data.
 * - $type: the media entity type, it can be Series, Movie, Event or empty.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the schedule item. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_tv_schedule_json_array()
 * @see template_process()
 *
 * @ingroup themeable
 */
if (empty($series_title)) :
  $series_title = $media_entity_title;
  $media_entity_title = '';
endif;
?>
<div class="schedule-row ">
  <div class="time ">
    <?php print $start_time;?>
  </div>
  <div class="image"></div>
  <div class="details">
    <p class="series"><?php print $series_title;?></p>
    <p class="episode"><?php print $media_entity_title;?></p>
    <p class="description"><?php print $synopsis;?></p>
  </div>
</div>
