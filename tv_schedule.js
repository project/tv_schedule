(function ($) {
  Drupal.behaviors.tv_schedule = {
    attach: function (context, settings) {
      $('#use-ajax-tv_schedule-select').change(function() {
        var element_settings = {};
        // Ajax submits specified in this manner automatically submit to the
        // normal form action.
        element_settings.url = '/' + $('#use-ajax-tv_schedule-select option:selected').val();
        element_settings.event = 'click';
        element_settings.progress = { 'type': 'throbber' };
        var base = $(this).attr('id');
        Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
      });
      if (typeof $('#use-ajax-tv_schedule-select-timezone').selectmenu === 'function') {
        $('#use-ajax-tv_schedule-select-timezone').selectmenu({
          change: function(event, ui) {
            var element_settings = {};
            // Ajax submits specified in this manner automatically submit to the
            // normal form action.
            var d = ui.item.value;
            $("#use-ajax-tv_schedule-select-timezone option").removeAttr("selected");
            $("#use-ajax-tv_schedule-select-timezone option[value='" + d + "']").attr("selected", "selected");
            $("#use-ajax-tv_schedule-select-timezone").change().blur();
        }
      });
      }
      $('#use-ajax-tv_schedule-select-timezone').bind('change blur', function() {
       var element_settings = {};
       if (!$('#use-ajax-tv_schedule-select-timezone-button .ui-selectmenu-text').length) {
         element_settings.url = '/' + $('#use-ajax-tv_schedule-select-timezone option:selected').val();
       }
       else {
         element_settings.url = '/' + $('#use-ajax-tv_schedule-select-timezone option:contains("' + $('#use-ajax-tv_schedule-select-timezone-button .ui-selectmenu-text').text() + '")').val();
       }
       element_settings.event = 'change blur click';
       element_settings.progress = { 'type': 'throbber' };
       var base = $(this).attr('id');
        //alert(JSON.stringify(element_settings) + " " + base);
       Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
       });
      $('#use-ajax-tv_schedule-select-filter').bind('change blur', function() {
        var element_settings = {};
        // Ajax submits specified in this manner automatically submit to the
        // normal form action.
        element_settings.url = '/' + $('#use-ajax-tv_schedule-select-filter option:selected').val();
        element_settings.event = 'change blur click';
        element_settings.progress = { 'type': 'throbber' };
        var base = $(this).attr('id');
        Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
      });
    }
  };
})(jQuery);
