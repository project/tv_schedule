<?php

/**
 * @file
 * Describe hooks provided by the TV Schedule module.
 */

/**
 * Alter the json data from schedule service.
 *
 * This hook should be placed in custom module or theme and it will be
 * auto-loaded.
 *
 * Structure of the initial schedule data in array format:
 * "Corus_TV_Schedules" : [
 *   {
 *     "Schedule_Line_Item": {
 *       "Channel Name": "W Network",
 *       "Schedule Date": "2015-02-14",
 *       "Start Time": "00:00:00",
 *       "Synopsis": "",
 *       "Duration": "7 200",
 *       "End Time": "02:00:00",
 *       "Program Type": "Movie",
 *       "Cast String": "",
 *       "CC": "N",
 *       "Director String": "",
 *       "DV": "N",
 *       "Episode ID": "175951",
 *       "Episode Number": "",
 *       "Genre Name": "IOM Genre",
 *       "House Media": "HD00692",
 *       "MPAA Rating": "PG",
 *       "Premiere": "Y",
 *       "Program ID": "175951",
 *       "Title": "I Hate Valentine's Day",
 *       "Version ID": "224 704",
 *       "Version Number": "1",
 *       "Year of Production": "2009",
 *       "Series Title": ""
 *     }
 *   }
 */
function hook_tv_schedule_data_alter(&$schedules) {
  foreach ($schedules as $key1 => $json) {
    foreach ($json as $key2 => $schedule) {
      foreach ($schedule as $key3 => $sch) {
        $schedules[$key1][$key2][$key3]['Start Time'] = tv_schedule_convert_time($sch['Start Time']);
      }
    }
  }
}
