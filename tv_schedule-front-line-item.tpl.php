<?php

/**
 * @file
 * Default theme implementation to display a schedule line item.
 *
 * Available variables:
 * - $series_title: the series title of the schedule line item.
 * - $media_entity_title episode title or movie title
 * - $start_time: start time of the show.
 * - $element: an array of raw data. $element['child'] contain all the raw data.
 * - $duration: time in seconds for the schedule item.
 * - $slot_class: class for the schedule item. There are 5 time of classes.
 * -    'half-hour'
 * -    'one-hour'
 * -    'one-hour-half'
 * -    'two-hour'
 * -    'two-hour-over'
 * - $start_time_arr is an array contains two item the time in hour and minutes
 *   format and ante.
 * - $type: the media entity type, it can be Series, Movie, Event or empty.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the schedule item. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_tv_schedule_front_json_array()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="schedule-front-item <?php print $slot_class; ?>">
  <div class="details">
    <?php print empty($series_title) ? $media_entity_title : $series_title;?>
  </div>
  <div>
    <?php print $start_time_arr[0];?>
    <span><?php print $start_time_arr[1];?></span>
  </div>
</div>
