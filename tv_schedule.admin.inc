<?php

/**
 * @file
 * TV schedule
 *
 * contains all forms and saving function for the schedule UI
 */

/**
 * Defines a settings form.
 */
function tv_schedule_settings($form) {
  $form['tv_schedule_fieldset1'] = array(
    '#type' => 'fieldset',
    '#title' => t('TV Schedule Settings'),
  );
  $form['tv_schedule_fieldset1']['tv_schedule_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Schedule internal name'),
    '#default_value' => variable_get('tv_schedule_name'),
  );
  $form['tv_schedule_fieldset1']['tv_schedule_remote_schedule_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Internal field name for remote schedule id'),
    '#default_value' => variable_get('tv_schedule_remote_schedule_id'),
  );
  $form['tv_schedule_fieldset1']['tv_schedule_total_days'] = array(
    '#type' => 'textfield',
    '#title' => t('How many days of schedule?'),
    '#default_value' => variable_get('tv_schedule_total_days', 7),
  );
  $form['tv_schedule_fieldset1']['tv_schedule_channel'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the channel'),
    '#default_value' => variable_get('tv_schedule_channel', ''),
    '#required' => TRUE,
    '#description' => t('We use this name to fetch the Json file from server. So, the name need to be same as the one used when naming the schedule json data file.'),
  );
  $form['tv_schedule_fieldset1']['tv_schedule_service'] = array(
    '#type' => 'textfield',
    '#title' => t('Location of the schedule service JSON files'),
    '#default_value' => variable_get('tv_schedule_service', ''),
    '#required' => TRUE,
    '#description' => t('We use this name to find the Json file from server. Provide the url of the folder without trail slash.'),
  );
  $form['tv_schedule_fieldset1']['tv_schedule_time_zones'] = array(
    '#type' => 'textarea',
    '#title' => t('Setup time zones that will be show up on the site'),
    '#default_value' => variable_get('tv_schedule_time_zones'),
    '#description' => t('The possible values this field can contain. Enter one value per line, in the format key|label. The key is the stored value. The label will be used in displayed values and edit forms. The label is optional: if a line contains a single string, it will be used as key and label.'),
  );
  $form['tv_schedule_fieldset1']['tv_schedule_css'] = array(
    '#type' => 'checkbox',
    '#title' => 'Add demo css',
    '#default_value' => variable_get('tv_schedule_css', 1),
    '#weight' => 1,
  );
  return system_settings_form($form);
}
