<?php

/**
 * @file
 * Default theme implementation to display whole schedule line items.
 *
 * Available variables:
 * - $list: An rendered array of schedule items. Just print them
 *   all.
 * - $dates_link: an rendered array of selectable date in link format.
 * - $dates_select: an rendered array of selectable date in HTML select format.
 * - $element: an array of schedule items. Use other template to modify schedule
 *   line items.
 * - $schedule_date: an array of all selectable data for schedule. use other
 *   template to customize them.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the schedule item. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_tv_schedule_render_array()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id='timezone'><?php print $timezone;?></div>
<div id='schedule-dates-select'><?php print $dates_select;?></div>
<div id='schedule-dates'><ul><?php print $dates_link;?></ul></div>
<div id='schedule-dates-page'><?php print $page_link;?></div>
<div id='schedule'><?php print $list;?></div>
<div id='filter'><?php print $filter;?></div>
