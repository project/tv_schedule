<?php

/**
 * @file
 * Default theme implementation to display whole schedule line items.
 *
 * Available variables:
 * - $list: An rendered array of schedule items. Just print them
 *   all.
 * - $element: an array of schedule items. Use other template to modify schedule
 *   line items.
 *   template to customize them.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the schedule item. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_tv_schedule_frontpage()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class='csch-frt-blk'>
<div class='csch-head'>
<h3>On Tonight</h3>
<div id='timezone'><?php print $timezone;?></div>
</div>
<div id='schedule'><?php print $list;?></div>
    <a class="see-full" href='/schedule'>See Full Schedule</a>
</div>
