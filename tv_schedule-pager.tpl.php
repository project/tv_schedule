<?php

/**
 * @file
 * Default theme implementation to display a single schedule date link.
 *
 * Available variables:
 * - $previous: either it is a active link or not, it is proint to previous day.
 * - $current: the text of current day.
 * - $next: either link or text, it is for next day.
 * - $element: an array of date link properties.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the schedule item. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_tv_schedule_block_pager()
 * @see template_process()
 *
 * @ingroup themeable
 */
 ?>
<div class='caddon_tv-schedule-page-style-links'>
<div class='caddon_tv-schedule-link-previous'><?php print $previous; ?></div>
<div class='caddon_tv-schedule-current'><?php print $current; ?></div>
<div class='caddon_tv-schedule-link-next'><?php print $next; ?></div>
</div>
