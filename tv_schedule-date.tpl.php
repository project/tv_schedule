<?php

/**
 * @file
 * Default theme implementation to display a single schedule date link.
 *
 * Available variables:
 * - $content: An rendered array of a date link. Just print them.
 * - $element: an array of date link properties.
 * - $href: the link url
 * - $linkid: the unique html id for the link element.
 * - $data: the additional data added when we preprocess the element.
 * - $options: the link attributes
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the schedule item. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_tv_schedule_link()
 * @see template_process()
 *
 * @ingroup themeable
 */
 ?>
<li class='caddon_tvs'>
<?php
$text = "{$data['day']} {$data['date']}";
print l($text, $href, $options);
?></li>
